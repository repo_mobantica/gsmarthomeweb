package com.gsmarthome.repository;

import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.model.UserDetails;

public interface UserRepository extends CrudRepository<UserDetails, Integer> {

}
