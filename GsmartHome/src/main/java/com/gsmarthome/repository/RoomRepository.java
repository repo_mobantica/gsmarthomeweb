package com.gsmarthome.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.model.Home;
import com.gsmarthome.model.Room;

public interface RoomRepository extends CrudRepository<Room, Integer> {

	List<Room> findByHome(Home id);
}
