package com.gsmarthome.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.model.Home;

public interface HomeRepository extends CrudRepository<Home, Integer> {

	Home findById(long id);
}
