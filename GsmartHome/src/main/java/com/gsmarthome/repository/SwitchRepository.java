package com.gsmarthome.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.model.Home;
import com.gsmarthome.model.Room;
import com.gsmarthome.model.Switch;

public interface SwitchRepository extends CrudRepository<Switch, Integer> {


	List<Switch> findByIotProductRoomHome(Home id);
	
	List<Switch> findByIotProductRoom(Room id);
}
