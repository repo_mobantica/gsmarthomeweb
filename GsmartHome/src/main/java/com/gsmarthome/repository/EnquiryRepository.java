package com.gsmarthome.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.model.Enquiry;

public interface EnquiryRepository extends CrudRepository<Enquiry, Integer>{

	//List<Enquiry> findByMessageType();
	List<Enquiry> findAll();
	Enquiry findById(Long id);
	List<Enquiry> findByStatus(int i);
	
	List<Enquiry> findByMessageType(int messageType);
	
	List<Enquiry> findByMessageTypeAndStatus(int messageType, int status);
}
