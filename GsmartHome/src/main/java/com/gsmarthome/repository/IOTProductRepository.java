package com.gsmarthome.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.model.Home;
import com.gsmarthome.model.IOTProduct;

public interface IOTProductRepository extends CrudRepository<IOTProduct, Integer> {

	List<IOTProduct> findByRoomHome(Home home);
}
