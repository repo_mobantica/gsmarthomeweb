package com.gsmarthome.repository;

import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.model.VersionType;

public interface VersionTypeRepository  extends CrudRepository<VersionType, Integer>{

	VersionType findById(int id);
}
