package com.gsmarthome.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.model.Login;

public interface AdminRepository extends CrudRepository<Login, Integer>{

	List<Login> findByStatus(int i);
	
	Login findById(int id);
	
	Login findByUsername(String username);
}
