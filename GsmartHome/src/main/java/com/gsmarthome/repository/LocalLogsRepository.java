package com.gsmarthome.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.model.LocalLogs;

public interface LocalLogsRepository extends CrudRepository<LocalLogs, Integer>{

	
	List findByHomeId(long homeId);
}
