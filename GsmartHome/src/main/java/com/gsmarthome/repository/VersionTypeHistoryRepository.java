package com.gsmarthome.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.model.VersionTypeHistory;

public interface VersionTypeHistoryRepository extends CrudRepository<VersionTypeHistory, Integer>{

	List findAll();
}
