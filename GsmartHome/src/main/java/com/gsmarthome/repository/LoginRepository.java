package com.gsmarthome.repository;

import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.model.Login;

public interface LoginRepository extends CrudRepository<Login, Integer> {

	Login findByUsernameAndPassword(String username, String password);

/*	void findOne(int id);

	void delete(int id);*/

}
