package com.gsmarthome.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.model.Enquiry;
import com.gsmarthome.model.EnquiryHistories;

public interface EnquiryHistoriesRepository  extends CrudRepository<EnquiryHistories, Integer>{

	//List<EnquiryHistories> findByEnquiryId(long id);

	
	List<EnquiryHistories> findByEnquiryDetails(Enquiry enquiry);
}
