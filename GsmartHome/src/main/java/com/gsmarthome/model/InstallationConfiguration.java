package com.gsmarthome.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class InstallationConfiguration {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
	
	@Column(length=1000000)
	private String configurationString;
	

	public String getConfigurationString() {
		return configurationString;
	}



	public void setConfigurationString(String configurationString) {
		this.configurationString = configurationString;
	}



	public InstallationConfiguration(String configurationString) {
		this.configurationString = configurationString;

	}	


}
