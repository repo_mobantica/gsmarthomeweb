package com.gsmarthome.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
public class SwitchType {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String switchTypeName;
	private String switchIdentifire;
	private String dimmerStatus;

	@Lob
	private byte[] onImage;

	@Lob
	private byte[] offImage;

	public String getSwitchIdentifire() {
		return switchIdentifire;
	}

	public void setSwitchIdentifire(String switchIdentifire) {
		this.switchIdentifire = switchIdentifire;
	}

	public SwitchType() {
		super();
	}

	public SwitchType(Long id, String switchTypeName) {
		super();
		this.id = id;
		this.switchTypeName = switchTypeName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSwitchTypeName() {
		return switchTypeName;
	}

	public void setSwitchTypeName(String switchTypeName) {
		this.switchTypeName = switchTypeName;
	}

	public byte[] getOnImage() {
		return onImage;
	}

	public void setOnImage(byte[] onImage) {
		this.onImage = onImage;
	}

	public byte[] getOffImage() {
		return offImage;
	}

	public void setOffImage(byte[] offImage) {
		this.offImage = offImage;
	}

	public String getDimmerStatus() {
		return dimmerStatus;
	}

	public void setDimmerStatus(String dimmerStatus) {
		this.dimmerStatus = dimmerStatus;
	}

	@Override
	public String toString() {
		return "SwitchType [id=" + id + ", switchTypeName=" + switchTypeName + "]";
	}
}
