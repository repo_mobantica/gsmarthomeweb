package com.gsmarthome.model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Entity;
@Entity
public class EnquiryHistories {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	private String issue_Action;
	private String created_date;
	private int status;
	
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "ENQUIRYID", nullable = false)
	private Enquiry enquiryDetails;
	
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "LOGINID", nullable = false)
	private Login loginDetails;

	@Transient
	private String stringStatus;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIssue_Action() {
		return issue_Action;
	}

	public void setIssue_Action(String issue_Action) {
		this.issue_Action = issue_Action;
	}

	public String getCreated_date() {
		return created_date;
	}

	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}

	public Enquiry getEnquiryDetails() {
		return enquiryDetails;
	}

	public void setEnquiryDetails(Enquiry enquiryDetails) {
		this.enquiryDetails = enquiryDetails;
	}

	public Login getLoginDetails() {
		return loginDetails;
	}

	public void setLoginDetails(Login loginDetails) {
		this.loginDetails = loginDetails;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getStringStatus() {
		return stringStatus;
	}

	public void setStringStatus(String stringStatus) {
		this.stringStatus = stringStatus;
	}
	
}
