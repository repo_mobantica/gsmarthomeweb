package com.gsmarthome.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class RemoteDetails {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;	
	private String remoteBrandName;
	private String modelNumber;
	private String remoteType;
	
	@Column(length=1000000)
	private String remoteCommandInfo;
	
	@ManyToOne
	@JoinColumn(name = "HOMEID")
	private Home home;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRemoteBrandName() {
		return remoteBrandName;
	}

	public void setRemoteBrandName(String remoteBrandName) {
		this.remoteBrandName = remoteBrandName;
	}

	public String getModelNumber() {
		return modelNumber;
	}

	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}

	public String getRemoteType() {
		return remoteType;
	}

	public void setRemoteType(String remoteType) {
		this.remoteType = remoteType;
	}

	public String getRemoteCommandInfo() {
		return remoteCommandInfo;
	}

	public void setRemoteCommandInfo(String remoteCommandInfo) {
		this.remoteCommandInfo = remoteCommandInfo;
	}

	public Home getHome() {
		return home;
	}

	public void setHome(Home home) {
		this.home = home;
	}

	@Override
	public String toString() {
		return "RemoteDetails [id=" + id + ", remoteBrandName=" + remoteBrandName + ", modelNumber=" + modelNumber
				+ ", remoteType=" + remoteType + ", remoteCommandInfo=" + remoteCommandInfo + ", home=" + home + "]";
	}	
	
}
