package com.gsmarthome.model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


import java.util.Date;

import javax.persistence.Entity;
@Entity
public class LocalLogs {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	private int messageType; //1. reboot, 2. checklocalconnection, 3. deploywar file
	private String message;
	private long homeId;
	private String created_date;
	
	@Transient
	private String messageTypeString;
	@Transient
	private String stringDate;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public int getMessageType() {
		return messageType;
	}
	public void setMessageType(int messageType) {
		this.messageType = messageType;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public long getHomeId() {
		return homeId;
	}
	public void setHomeId(long homeId) {
		this.homeId = homeId;
	}
	public String getCreated_date() {
		return created_date;
	}
	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}
	public String getMessageTypeString() {
		return messageTypeString;
	}
	public void setMessageTypeString(String messageTypeString) {
		this.messageTypeString = messageTypeString;
	}
	public String getStringDate() {
		return stringDate;
	}
	public void setStringDate(String stringDate) {
		this.stringDate = stringDate;
	}
}
