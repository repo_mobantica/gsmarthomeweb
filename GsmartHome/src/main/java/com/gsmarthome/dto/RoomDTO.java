package com.gsmarthome.dto;

public class RoomDTO {

	private String id;
	private String roomName;
	private String roomType;
	private String roomIdentifier;
	private String iotproductName;
	private String iotproductNumber;
	private String homeId;
	private String homeName;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRoomName() {
		return roomName;
	}
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
	public String getRoomType() {
		return roomType;
	}
	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}
	public String getRoomIdentifier() {
		return roomIdentifier;
	}
	public void setRoomIdentifier(String roomIdentifier) {
		this.roomIdentifier = roomIdentifier;
	}
	public String getIotproductName() {
		return iotproductName;
	}
	public void setIotproductName(String iotproductName) {
		this.iotproductName = iotproductName;
	}
	public String getIotproductNumber() {
		return iotproductNumber;
	}
	public void setIotproductNumber(String iotproductNumber) {
		this.iotproductNumber = iotproductNumber;
	}
	public String getHomeId() {
		return homeId;
	}
	public void setHomeId(String homeId) {
		this.homeId = homeId;
	}
	public String getHomeName() {
		return homeName;
	}
	public void setHomeName(String homeName) {
		this.homeName = homeName;
	}
	
}
