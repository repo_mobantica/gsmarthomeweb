package com.gsmarthome.dto;

public class EnquiryHistoriesDTO {

	private int loginId;
	private long enquiryId;
	private long userid;
	private String issue_Action;
	private String created_date;
	private int status;
	public int getLoginId() {
		return loginId;
	}
	public void setLoginId(int loginId) {
		this.loginId = loginId;
	}
	public long getEnquiryId() {
		return enquiryId;
	}
	public void setEnquiryId(long enquiryId) {
		this.enquiryId = enquiryId;
	}
	
	public long getUserid() {
		return userid;
	}
	public void setUserid(long userid) {
		this.userid = userid;
	}
	public String getIssue_Action() {
		return issue_Action;
	}
	public void setIssue_Action(String issue_Action) {
		this.issue_Action = issue_Action;
	}
	public String getCreated_date() {
		return created_date;
	}
	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	
}
