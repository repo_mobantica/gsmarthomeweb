package com.gsmarthome.dto;

public class IOTProductDTO {


	private String id;
	private String iotproductName;
	private String iotproductNumber;
	private String roomId;
	private String roomName;
	private String homeId;
	private String homeName;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIotproductName() {
		return iotproductName;
	}
	public void setIotproductName(String iotproductName) {
		this.iotproductName = iotproductName;
	}
	public String getIotproductNumber() {
		return iotproductNumber;
	}
	public void setIotproductNumber(String iotproductNumber) {
		this.iotproductNumber = iotproductNumber;
	}
	public String getRoomId() {
		return roomId;
	}
	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}
	public String getRoomName() {
		return roomName;
	}
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
	public String getHomeId() {
		return homeId;
	}
	public void setHomeId(String homeId) {
		this.homeId = homeId;
	}
	public String getHomeName() {
		return homeName;
	}
	public void setHomeName(String homeName) {
		this.homeName = homeName;
	}
	
	
}
