package com.gsmarthome.dto;

public class SwitchDTO {

	private String id;
	private String switchNumber;
	private String switchType;
	private String switchName;
	private String iotproductName;
	private String iotproductNumber;
	private String roomId;
	private String roomName;
	private String homeId;
	private String homeName;
	private String switchStatus;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSwitchNumber() {
		return switchNumber;
	}
	public void setSwitchNumber(String switchNumber) {
		this.switchNumber = switchNumber;
	}
	public String getSwitchType() {
		return switchType;
	}
	public void setSwitchType(String switchType) {
		this.switchType = switchType;
	}
	public String getSwitchName() {
		return switchName;
	}
	public void setSwitchName(String switchName) {
		this.switchName = switchName;
	}
	public String getIotproductName() {
		return iotproductName;
	}
	public void setIotproductName(String iotproductName) {
		this.iotproductName = iotproductName;
	}
	public String getIotproductNumber() {
		return iotproductNumber;
	}
	public void setIotproductNumber(String iotproductNumber) {
		this.iotproductNumber = iotproductNumber;
	}
	public String getRoomId() {
		return roomId;
	}
	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}
	public String getRoomName() {
		return roomName;
	}
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
	public String getHomeId() {
		return homeId;
	}
	public void setHomeId(String homeId) {
		this.homeId = homeId;
	}
	public String getHomeName() {
		return homeName;
	}
	public void setHomeName(String homeName) {
		this.homeName = homeName;
	}
	public String getSwitchStatus() {
		return switchStatus;
	}
	public void setSwitchStatus(String switchStatus) {
		this.switchStatus = switchStatus;
	}
}
