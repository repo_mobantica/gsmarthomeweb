package com.gsmarthome.services;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gsmarthome.model.DeviceDetails;
import com.gsmarthome.model.Login;
import com.gsmarthome.repository.AdminRepository;
@Service
@Transactional
public class AdminService {

	@Autowired
	AdminRepository adminRepository;
	public List<Login> AllAdminList()
	{
		List<Login> adminList=new ArrayList<Login>();
		
		for(Login admin:adminRepository.findAll())
		{
			adminList.add(admin);
		}
		
		return adminList;
	}
	public void savenewAdmin(Login login) {
		// TODO Auto-generated method stub
		adminRepository.save(login);
	}
	
}
