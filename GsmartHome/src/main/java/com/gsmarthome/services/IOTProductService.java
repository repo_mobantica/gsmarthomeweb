package com.gsmarthome.services;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.gsmarthome.model.IOTProduct;
import com.gsmarthome.repository.IOTProductRepository;

@Service
@Transactional
public class IOTProductService {


	private final IOTProductRepository iotproductRepository;
	
	public IOTProductService(IOTProductRepository iotproductRepository)
	{
		this.iotproductRepository=iotproductRepository;
	}

	public List<IOTProduct> AllIOTProductList()
	{
		List<IOTProduct> iotproductList=new ArrayList<IOTProduct>();
		
		for(IOTProduct iotproduct:iotproductRepository.findAll())
		{
			iotproductList.add(iotproduct);
		}
		
		return iotproductList;
	}
}
