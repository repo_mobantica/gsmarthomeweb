package com.gsmarthome.services;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gsmarthome.model.Enquiry;
import com.gsmarthome.model.Home;
import com.gsmarthome.model.UserDetails;
import com.gsmarthome.repository.EnquiryRepository;
import com.gsmarthome.repository.HomeRepository;

@Service
@Transactional
public class HomeService {


	private final HomeRepository homeRepository;

	@Autowired
	EnquiryRepository enquiryRepository;
	public long countHome()
	{
		return homeRepository.count();
	}

	public HomeService(HomeRepository homeRepository)
	{
		this.homeRepository=homeRepository;
	}

	public List<Home> AllHomeList()
	{
		List<Home> homeList=new ArrayList<Home>();
		for(Home home:homeRepository.findAll())
		{
			homeList.add(home);
		}
		return homeList;
	}

	public long countOpenTickets()
	{
		List<Enquiry> enquiryList=enquiryRepository.findByMessageTypeAndStatus(2,1);
		return enquiryList.size();
	}

	public long countNewEnquiry()
	{
		try {
			List<Enquiry> enquiryList=enquiryRepository.findByMessageTypeAndStatus(3,1);
			return enquiryList.size();
		}catch (Exception e) {
			return 1;
			// TODO: handle exception
		}
	}

}
