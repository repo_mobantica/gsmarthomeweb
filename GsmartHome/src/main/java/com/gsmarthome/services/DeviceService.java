package com.gsmarthome.services;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.gsmarthome.model.DeviceDetails;
import com.gsmarthome.model.Login;
import com.gsmarthome.repository.DeviceRepository;

@Service
@Transactional
public class DeviceService {


	private final DeviceRepository deviceRepository;
	
	public DeviceService(DeviceRepository deviceRepository)
	{
		this.deviceRepository=deviceRepository;
	}


	public List<DeviceDetails> AllDeviceList()
	{
		List<DeviceDetails> deviceList=new ArrayList<DeviceDetails>();
		
		for(DeviceDetails device:deviceRepository.findAll())
		{
			deviceList.add(device);
		}
		
		return deviceList;
	}
	
	

	public void saveDevice(DeviceDetails devicedetails)
	{
		deviceRepository.save(devicedetails);
		
	}
	
}
