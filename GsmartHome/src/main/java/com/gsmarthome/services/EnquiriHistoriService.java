package com.gsmarthome.services;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gsmarthome.dto.EnquiryHistoriesDTO;
import com.gsmarthome.model.Enquiry;
import com.gsmarthome.model.EnquiryHistories;
import com.gsmarthome.model.Login;
import com.gsmarthome.model.UserDetails;
import com.gsmarthome.repository.EnquiryHistoriesRepository;
import com.gsmarthome.repository.EnquiryRepository;
import java.text.ParseException;  
import java.text.SimpleDateFormat;  
import java.util.Date;  
import java.util.Locale;  
@Service
@Transactional
public class EnquiriHistoriService {

	@Autowired
	EnquiryHistoriesRepository enquiryhistoriesRepository;
	@Autowired
	EnquiryRepository enquiryRepository;


	public void saveEnquiryHistory(EnquiryHistoriesDTO enquiryhistoriesDTO) {
		// TODO Auto-generated method stub
		EnquiryHistories enquiryhistories=new EnquiryHistories();
		
		SimpleDateFormat  formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");  
	    
		Enquiry enquiry=new Enquiry();
		enquiry.setId(enquiryhistoriesDTO.getEnquiryId());
		/*
		UserDetails userDetails=new UserDetails();
		userDetails.setId(enquiryhistoriesDTO.getUserid());
		enquiry.setUserDetails(userDetails);
		enquiry.setStatus(2);
		*/
		
		Login login=new Login();
		login.setId(enquiryhistoriesDTO.getLoginId());

		enquiryhistories.setEnquiryDetails(enquiry);
		enquiryhistories.setIssue_Action(enquiryhistoriesDTO.getIssue_Action());
		enquiryhistories.setCreated_date(formatter.format(new Date()));
		enquiryhistories.setLoginDetails(login);
		enquiryhistories.setStatus(enquiryhistoriesDTO.getStatus());
		enquiryhistoriesRepository.save(enquiryhistories);
		//enquiryRepository.save(enquiry);
		
		Enquiry enquiry1 = enquiryRepository.findById(enquiryhistoriesDTO.getEnquiryId());
		enquiry1.setStatus(enquiryhistoriesDTO.getStatus());
		enquiryRepository.save(enquiry1);
		
		//enquiryRepository.updateEnquiry(enquiryhistoriesDTO.getEnquiryId(),2);
	}

}
