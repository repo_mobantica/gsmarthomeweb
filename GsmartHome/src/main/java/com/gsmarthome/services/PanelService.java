package com.gsmarthome.services;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.gsmarthome.model.Panel;
import com.gsmarthome.repository.PanelRepository;

@Service
@Transactional
public class PanelService {


	private final PanelRepository panelRepository;
	
	public PanelService(PanelRepository panelRepository)
	{
		this.panelRepository=panelRepository;
	}

	public List<Panel> AllHomeList()
	{
		List<Panel> panelList=new ArrayList<Panel>();
		
		for(Panel panel:panelRepository.findAll())
		{
			panelList.add(panel);
		}
		
		return panelList;
	}
}
