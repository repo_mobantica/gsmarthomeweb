package com.gsmarthome.services;


import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gsmarthome.dto.EnquiryHistoriesDTO;
import com.gsmarthome.model.Enquiry;
import com.gsmarthome.model.EnquiryHistories;
import com.gsmarthome.model.Login;
import com.gsmarthome.model.UserDetails;
import com.gsmarthome.repository.EnquiryHistoriesRepository;
import com.gsmarthome.repository.EnquiryRepository;
import java.text.ParseException;  
import java.text.SimpleDateFormat;  
import java.util.Date;
import java.util.List;
import java.util.Locale;  
@Service
@Transactional
public class EnquiryService {

	@Autowired
	EnquiryHistoriesRepository enquiryhistoriesRepository;
	@Autowired
	EnquiryRepository enquiryRepository;

	public Enquiry getEnquiry(long id) {

		Enquiry enquiryDetails=enquiryRepository.findById(id);

		if(enquiryDetails.getMessageType()==1)
		{
			enquiryDetails.setMsgType("Feedback");
		}
		else if(enquiryDetails.getMessageType()==2)
		{
			enquiryDetails.setMsgType("Complaint");
		}
		else
		{
			enquiryDetails.setMsgType("Enquiry");
		}

		if(enquiryDetails.getStatus()==1)
		{
			enquiryDetails.setMsgStatus("New");
		}
		else if(enquiryDetails.getStatus()==2)
		{
			enquiryDetails.setMsgStatus("In-process");
		}
		else if(enquiryDetails.getStatus()==3)
		{
			enquiryDetails.setMsgStatus("Complete");
		}
			

		return enquiryDetails;
	}

	public List<Enquiry> getEnquiryList(int messageType) {

		List<Enquiry> enquiryList=enquiryRepository.findByMessageType(messageType);

		for(int i=0;i<enquiryList.size();i++)
		{
			if(enquiryList.get(i).getMessageType()==1)
			{
				enquiryList.get(i).setMsgType("Feedback");
			}
			else if(enquiryList.get(i).getMessageType()==2)
			{
				enquiryList.get(i).setMsgType("Complaint");
			}
			else
			{
				enquiryList.get(i).setMsgType("Enquiry");
			}

			if(enquiryList.get(i).getStatus()==1)
			{
				enquiryList.get(i).setMsgStatus("New");
			}
			else if(enquiryList.get(i).getStatus()==2)
			{
				enquiryList.get(i).setMsgStatus("In-process");
			}
			else if(enquiryList.get(i).getStatus()==3)
			{
				enquiryList.get(i).setMsgStatus("Complete");
			}
		}			

		return enquiryList;
	}

	public List<Enquiry> getEnquiryListByfindByMessageTypeAndStatus(int messageType,int status) {

		List<Enquiry> enquiryList=enquiryRepository.findByMessageTypeAndStatus(messageType,status);

		for(int i=0;i<enquiryList.size();i++)
		{
			if(enquiryList.get(i).getMessageType()==1)
			{
				enquiryList.get(i).setMsgType("Feedback");
			}
			else if(enquiryList.get(i).getMessageType()==2)
			{
				enquiryList.get(i).setMsgType("Complaint");
			}
			else
			{
				enquiryList.get(i).setMsgType("Enquiry");
			}

			if(enquiryList.get(i).getStatus()==1)
			{
				enquiryList.get(i).setMsgStatus("New");
			}
			else if(enquiryList.get(i).getStatus()==2)
			{
				enquiryList.get(i).setMsgStatus("In-process");
			}
			else if(enquiryList.get(i).getStatus()==3)
			{
				enquiryList.get(i).setMsgStatus("Complete");
			}
		}			

		return enquiryList;
	}
}
