package com.gsmarthome.services;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.gsmarthome.model.Login;
import com.gsmarthome.repository.LoginRepository;

@Service
@Transactional
public class LoginService {
	
	private final LoginRepository loginRepository;
	
	public LoginService(LoginRepository loginRepository)
	{
		this.loginRepository=loginRepository;
	}
	
	
	public void saveLogin(Login login)
	{
		loginRepository.save(login);
		
	}
	public Login view(String username, String password)
	{
		return loginRepository.findByUsernameAndPassword(username, password);
	}
	
	public List<Login> showAllLogin()
	{
		List<Login> userList=new ArrayList<Login>();
		
		for(Login login:loginRepository.findAll())
		{
			userList.add(login);
		}
		
		return userList;
	}
	
	public void deleteLogin(int id)
	{
	//	loginRepository.delete(id);
	}
	
	public void edit(int id)
	{
		//loginRepository.findOne(id);
	}
}
