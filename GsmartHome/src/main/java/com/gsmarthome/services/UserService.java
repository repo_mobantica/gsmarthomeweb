package com.gsmarthome.services;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.gsmarthome.model.Login;
import com.gsmarthome.model.UserDetails;
import com.gsmarthome.repository.UserRepository;

@Service
@Transactional
public class UserService {

	private final UserRepository userRepository;
	
	public UserService(UserRepository userRepository)
	{
		this.userRepository=userRepository;
	}
	
	public long countUsers()
	{
		
		return userRepository.count();
	}
	
	public void saveUser(UserDetails user)
	{
		
		userRepository.save(user);
		
	}

	public List<UserDetails> AllUserList()
	{
		List<UserDetails> userList=new ArrayList<UserDetails>();
		
		for(UserDetails user:userRepository.findAll())
		{
			userList.add(user);
		}
		
		return userList;
	}
	
}
