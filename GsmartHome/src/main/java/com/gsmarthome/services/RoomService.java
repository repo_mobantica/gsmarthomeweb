package com.gsmarthome.services;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.gsmarthome.model.Room;
import com.gsmarthome.repository.RoomRepository;

@Service
@Transactional
public class RoomService {


	private final RoomRepository roomRepository;
	
	public RoomService(RoomRepository roomRepository)
	{
		this.roomRepository=roomRepository;
	}
	

	public List<Room> AllRoomList()
	{
		List<Room> roomList=new ArrayList<Room>();
		
		for(Room room:roomRepository.findAll())
		{
			roomList.add(room);
		}
		
		return roomList;
	}	
}
