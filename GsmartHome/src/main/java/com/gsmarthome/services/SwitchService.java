package com.gsmarthome.services;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.gsmarthome.model.Switch;
import com.gsmarthome.repository.SwitchRepository;

@Service
@Transactional
public class SwitchService {


	private final SwitchRepository switchRepository;
	
	public SwitchService(SwitchRepository switchRepository)
	{
		this.switchRepository=switchRepository;
	}

	public List<Switch> AllSwitchList()
	{
		List<Switch> switchList=new ArrayList<Switch>();
		
		for(Switch Switch:switchRepository.findAll())
		{
			switchList.add(Switch);
		}
		
		return switchList;
	}
	
}
