package com.gsmarthome;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;

import com.amazonaws.util.json.JSONObject;

import mqttconnection.MqttlCentralConnection;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import com.amazonaws.util.json.JSONArray;
import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.net.URLConnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;



@Service
public class ScheduledTasks {

	private static final SimpleDateFormat dateFormatByDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:00");
	private static final SimpleDateFormat dateFormatByTime = new SimpleDateFormat("HH:mm:00");


	@Async
	@Scheduled(fixedRate = 1000*20)
	public void call() throws IOException  {
		
		try {
			MqttlCentralConnection.getObj().getInternetMqttConnection();
		}
		catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}


	}

	
}
