package com.gsmarthome.commanservices;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.net.HttpURLConnection;

import com.amazonaws.util.json.JSONObject;

public class PostCollection {


	public static String postCommanRestApi(String url, JSONObject jsonData)
			throws java.io.IOException
	{
		try {
			StringBuffer response = null;
			java.net.URL obj = new java.net.URL(url);
			HttpURLConnection con = (HttpURLConnection)obj.openConnection();

			con.setRequestMethod("POST");
			con.setRequestProperty("User-Agent", "Mozilla/5.0");
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
			con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("X-AUTH-TOKEN", "eyJ1c2VybmFtZSI6ImJhaGlybmF0aEBtb2JhbnRpY2EuY29tIiwiZXhwaXJlcyI6MTYxODQ5NDc3MzMwMSwiY3VzdG9tZXJUeXBlIjowLCJ1c2VySWQiOjEzM30=.wCCpBMF+K6DGV9d+kOdNUYYITrKla9goWW2Q6+63S/g=");

			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(jsonData.toString());
			wr.flush();
			wr.close();

			BufferedReader in = new BufferedReader(new java.io.InputStreamReader(con.getInputStream()));

			response = new StringBuffer();
			String output; while ((output = in.readLine()) != null) { 
				//String output;
				response.append(output);
			}
			in.close();
			return response.toString();
		}catch (Exception e) {
			e.printStackTrace();
			return "";
			// TODO: handle exception
		}
	}

}

