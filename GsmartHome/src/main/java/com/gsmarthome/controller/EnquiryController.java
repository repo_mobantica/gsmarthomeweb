package com.gsmarthome.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.gsmarthome.dto.EnquiryHistoriesDTO;
import com.gsmarthome.model.Enquiry;
import com.gsmarthome.model.EnquiryHistories;
import com.gsmarthome.model.Login;
import com.gsmarthome.repository.EnquiryHistoriesRepository;
import com.gsmarthome.repository.EnquiryRepository;
import com.gsmarthome.services.EnquiriHistoriService;
import com.gsmarthome.services.EnquiryService;

@Controller
public class EnquiryController {

	@Autowired
	EnquiryRepository enquiryRepository;
	@Autowired
	EnquiryHistoriesRepository enquiryhistoriesRepository;
	@Autowired
	EnquiriHistoriService enquirihistoriService;
	@Autowired
	EnquiryService enquiryService;


	@RequestMapping("/EnquiryMaster")
	public String EnquiryMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<Enquiry> enquiryList=enquiryService.getEnquiryList(3);
			model.addAttribute("enquiryList",enquiryList);
			return "EnquiryMaster";

		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return "Login";
		}
	}

	@RequestMapping("/EditEnquiry")
	public String EditEnquiry(@RequestParam("id") long id, ModelMap model)
	{
		try {
			Enquiry enquiryDetails=enquiryService.getEnquiry(id);
			List<EnquiryHistories> enquiryhistoriesList= enquiryhistoriesRepository.findByEnquiryDetails(enquiryDetails);

			for(int i=0;i<enquiryhistoriesList.size();i++)
			{
				if(enquiryhistoriesList.get(i).getStatus()==1)
				{
					enquiryhistoriesList.get(i).setStringStatus("New");
				}
				else  if(enquiryhistoriesList.get(i).getStatus()==2)
				{
					enquiryhistoriesList.get(i).setStringStatus("In-process");
				}
				else if(enquiryhistoriesList.get(i).getStatus()==3)
				{
					enquiryhistoriesList.get(i).setStringStatus("Complete");
				}
			}


			model.addAttribute("enquiryhistoriesList", enquiryhistoriesList);
			model.addAttribute("enquiryDetails", enquiryDetails);
			return "EditEnquiry";

		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return "Login";
		}
	}

	@RequestMapping(value = "/EditEnquiry", method = RequestMethod.POST)
	public String EditEnquiry(@ModelAttribute EnquiryHistoriesDTO enquiryhistoriesDTO, @RequestParam("priviousStatus") int priviousStatus, @RequestParam("priviousMsgType") int priviousMsgType, Model model)
	{
		try {
			enquirihistoriService.saveEnquiryHistory(enquiryhistoriesDTO);

			if(priviousMsgType==1)
			{
				List<Enquiry> enquiryList=enquiryService.getEnquiryList(1);
				model.addAttribute("enquiryList",enquiryList);
				return "FeedbackMaster";
			}
			else if(priviousMsgType==2)
			{
				if(priviousStatus==1)
				{
					List<Enquiry> enquiryList=enquiryService.getEnquiryListByfindByMessageTypeAndStatus(2,1);
					model.addAttribute("enquiryList",enquiryList);
					return "ComplaintMaster";
				}
				else if(priviousStatus==2)
				{
					List<Enquiry> enquiryList=enquiryService.getEnquiryListByfindByMessageTypeAndStatus(2,2);
					model.addAttribute("enquiryList",enquiryList);
					return "InprogressComplaintMaster";
				}
				else
				{
					List<Enquiry> enquiryList=enquiryService.getEnquiryListByfindByMessageTypeAndStatus(2,3);
					model.addAttribute("enquiryList",enquiryList);
					return "CompletedComplaintMaster";
				}
			}
			else
			{
				List<Enquiry> enquiryList=enquiryService.getEnquiryList(3);
				model.addAttribute("enquiryList",enquiryList);
				return "EnquiryMaster";
			}

		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return "Login";
		}
	}

	@RequestMapping("/FeedbackMaster")
	public String FeedbackMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<Enquiry> enquiryList=enquiryService.getEnquiryList(1);
			model.addAttribute("enquiryList",enquiryList);
			return "FeedbackMaster";

		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return "Login";
		}
	}

	@RequestMapping("/ComplaintMaster")
	public String ComplaintMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<Enquiry> enquiryList=enquiryService.getEnquiryListByfindByMessageTypeAndStatus(2,1);
			model.addAttribute("enquiryList",enquiryList);
			return "ComplaintMaster";

		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return "Login";
		}
	}

	@RequestMapping("/InprogressComplaintMaster")
	public String InprogressComplaintMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<Enquiry> enquiryList=enquiryService.getEnquiryListByfindByMessageTypeAndStatus(2,2);
			model.addAttribute("enquiryList",enquiryList);
			return "InprogressComplaintMaster";

		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return "Login";
		}
	}
	@RequestMapping("/CompletedComplaintMaster")
	public String CompletedComplaintMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<Enquiry> enquiryList=enquiryService.getEnquiryListByfindByMessageTypeAndStatus(2,3);
			model.addAttribute("enquiryList",enquiryList);
			return "CompletedComplaintMaster";

		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return "Login";
		}
	}
}
