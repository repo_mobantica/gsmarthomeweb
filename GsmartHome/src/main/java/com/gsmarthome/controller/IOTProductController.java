package com.gsmarthome.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gsmarthome.dto.IOTProductDTO;
import com.gsmarthome.dto.RoomDTO;
import com.gsmarthome.model.Home;
import com.gsmarthome.model.IOTProduct;
import com.gsmarthome.model.Room;
import com.gsmarthome.services.HomeService;
import com.gsmarthome.services.IOTProductService;
import com.gsmarthome.repository.*;
@Controller
public class IOTProductController {


	@Autowired
	private HomeService homeService;
	@Autowired
	private IOTProductService iotproductService;
	@Autowired
	private IOTProductRepository iotproductRepository;

	@RequestMapping("/IOTProductMaster")
	public String IOTProductMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
		List<IOTProduct> iotproductList=iotproductService.AllIOTProductList();
		List<Home> homeList=homeService.AllHomeList();

		model.addAttribute("homeList",homeList);
		model.addAttribute("iotproductList",iotproductList);
		return "IOTProductMaster";
		
		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return "Login";
		}
	}
	
	

	@ResponseBody
	@RequestMapping("/getIotProductListByHome")
	public List<IOTProductDTO> getIotProductListByHome(@RequestParam("homeId") String homeId)
	{
		Home home =new Home();
		home.setId(Long.valueOf(homeId));
		List<IOTProduct> iotproductList1=new ArrayList<>();
		List<IOTProductDTO> iotproductList=new ArrayList<>(); 
		iotproductList1=iotproductRepository.findByRoomHome(home);
		IOTProductDTO iotproductDTO=new IOTProductDTO();
		for(int i=0;i<iotproductList1.size();i++)
		{
			 iotproductDTO=new IOTProductDTO();
			 iotproductDTO.setId(iotproductList1.get(i).getId()+"");
			 iotproductDTO.setIotproductNumber(iotproductList1.get(i).getIotProductNumber()+"");
			 iotproductDTO.setIotproductName(iotproductList1.get(i).getProductName());
			 iotproductDTO.setRoomId(iotproductList1.get(i).getRoom().getId()+"");
			 iotproductDTO.setRoomName(iotproductList1.get(i).getRoom().getRoomName());
			 iotproductDTO.setHomeId(iotproductList1.get(i).getRoom().getHome().getId()+"");
			 iotproductDTO.setHomeName(iotproductList1.get(i).getRoom().getHome().getHomeName());
			 iotproductList.add(iotproductDTO);
		}
		return iotproductList;
	}

}
