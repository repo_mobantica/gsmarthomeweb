package com.gsmarthome.controller;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.amazonaws.util.json.JSONObject;
import com.gsmarthome.commanservices.PostCollection;
import com.gsmarthome.model.LocalLogs;
import com.gsmarthome.repository.LocalLogsRepository;

import mqttconnection.MqttlCentralConnection;

@Controller
public class LocalHomeSetupController {

	@Autowired
	LocalLogsRepository localLogsRepository;

	@ResponseBody
	@RequestMapping(value="/connectMqttConnection",method=RequestMethod.POST)
	public void connectMqttConnection()
	{

		try {
			MqttlCentralConnection.getObj().getInternetMqttConnection();
		}
		catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}

		String url1="http://gsmarthome.genieiot.in/home/homeverification";	

		try {
			JSONObject postJsonData = new JSONObject();
			postJsonData.put("homeId", "1111");
			JSONObject jsonObject = new JSONObject(PostCollection.postCommanRestApi(url1, postJsonData));
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		//MqttlCentralConnection.getObj().getInternetMqttConnection();
	}

	@ResponseBody
	@RequestMapping(value="/rebootLocalHome",method=RequestMethod.POST)
	public void rebootLocalHome(@RequestParam("id") long id)
	{
		String topic="restartHomeId_"+id;
		System.out.println("rebootLocalHome =  "+id);
		String message1="wish1";
		MqttMessage message = new MqttMessage(message1.toString().getBytes());
		message.setQos(2);
		try {
			MqttlCentralConnection.getObj().getInternetMqttConnection().publish(topic, message);
		} catch (MqttException e) {
			e.printStackTrace();
		}
	}

	@ResponseBody
	@RequestMapping(value="/checkLocalConnection",method=RequestMethod.POST)
	public void checkLocalConnection(@RequestParam("id") long id)
	{
		String topic="restartHomeId_"+id;
		System.out.println("checkLocalConnection =  "+id);
		String message1="wish2";
		try {

			MqttMessage message = new MqttMessage(message1.toString().getBytes());
			message.setQos(2);
			MqttlCentralConnection.getObj().getInternetMqttConnection().publish(topic, message);
		} catch (MqttException e) {
			e.printStackTrace();
		}
	}

	@ResponseBody
	@RequestMapping(value="/deployWarFileInLocalHome",method=RequestMethod.POST)
	public void deployWarFileInLocalHome(@RequestParam("id") long id)
	{
		String topic="restartHomeId_"+id;
		System.out.println("deployWarFileInLocalHome =  "+id);
		String message1="wish3";
		MqttMessage message = new MqttMessage(message1.toString().getBytes());
		message.setQos(2);
		try {
			MqttlCentralConnection.getObj().getInternetMqttConnection().publish(topic, message);
		} catch (MqttException e) {
			e.printStackTrace();
		}
	}

	public void saveData()
	{
		System.out.println("In Save");
		try {
			LocalLogs LocalLogs=new LocalLogs();
			LocalLogs.setMessage("ABC");
			localLogsRepository.save(LocalLogs);
			System.out.println("INNNN");
		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}
}
