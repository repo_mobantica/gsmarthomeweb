package com.gsmarthome.controller;

import java.util.Date;
import java.util.List;
import java.text.DateFormat;  
import java.text.SimpleDateFormat;  
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gsmarthome.model.Home;
import com.gsmarthome.model.Login;
import com.gsmarthome.model.VersionType;
import com.gsmarthome.model.VersionTypeHistory;
import com.gsmarthome.repository.VersionTypeRepository;
import com.gsmarthome.repository.VersionTypeHistoryRepository;

@Controller
public class VersionTypeController {

	@Autowired
	private VersionTypeRepository versionTypeRepository;
	@Autowired
	private VersionTypeHistoryRepository versionTypeHistoryRepository;


	@RequestMapping("/VersionMaster")
	public String VersionMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			DateFormat dateFormat = new SimpleDateFormat("dd-mm-yyyy hh:mm:ss");  
			List<VersionTypeHistory> versionHistoryList=versionTypeHistoryRepository.findAll();
			for(int i=0;i<versionHistoryList.size();i++)
			{
				versionHistoryList.get(i).setStringDate(dateFormat.format(versionHistoryList.get(i).getCreateDate()));
			}

			model.addAttribute("versionHistoryList",versionHistoryList);
			return "VersionMaster";

		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return "Login";
		}
	}

	@RequestMapping("/AddVersion")
	public String AddVersion(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			return "AddVersion";

		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return "Login";
		}
	}

	@RequestMapping(value = "/AddVersion", method = RequestMethod.POST)
	public String AddVersion(@ModelAttribute VersionTypeHistory versiontypeHistory, Model model)
	{
		try {
			versiontypeHistory.setCreateDate(new Date());
			versionTypeHistoryRepository.save(versiontypeHistory);
			VersionType versionType=versionTypeRepository.findById(1);
			versionType.setVersionName(versiontypeHistory.getVersionName());
			versionType.setVersionType(versiontypeHistory.getVersionType());
			versionType.setCreateDate(new Date());
			versionTypeRepository.save(versionType);
			DateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy hh:mm:ss");
			List<VersionTypeHistory> versionHistoryList=versionTypeHistoryRepository.findAll();
			for(int i=0;i<versionHistoryList.size();i++)
			{
				versionHistoryList.get(i).setStringDate(dateFormat.format(versionHistoryList.get(i).getCreateDate()));
			}
			model.addAttribute("versionHistoryList",versionHistoryList);
			return "VersionMaster";

		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return "Login";
		}
	}

}
