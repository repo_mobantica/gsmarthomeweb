package com.gsmarthome.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gsmarthome.services.DeviceService;
import com.gsmarthome.services.HomeService;
import com.gsmarthome.services.UserService;

@Controller
public class DashboardController {

	@Autowired
	private DeviceService deviceService;

	@Autowired
	private UserService userService;

	@Autowired
	private HomeService homeService;
	
	@RequestMapping("/Dashboard")
	public String Dashboard(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
		long totalUsers=userService.countUsers();
		long totalHome=homeService.countHome();
		long openTickets=homeService.countOpenTickets();
		long newEnquiry=homeService.countNewEnquiry();
		
		model.addAttribute("newEnquiry",newEnquiry);
		model.addAttribute("openTickets",openTickets);
		model.addAttribute("totalHome",totalHome);
		model.addAttribute("totalUsers",totalUsers);
		return "Dashboard";
		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return "Login";
		}
	}
	
}
