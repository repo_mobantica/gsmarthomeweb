package com.gsmarthome.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gsmarthome.dto.RoomDTO;
import com.gsmarthome.dto.SwitchDTO;
import com.gsmarthome.model.Home;
import com.gsmarthome.model.Room;
import com.gsmarthome.model.Switch;
import com.gsmarthome.model.UserDetails;
import com.gsmarthome.services.HomeService;
import com.gsmarthome.services.RoomService;
import com.gsmarthome.repository.RoomRepository;

@Controller
public class RoomController {

	@Autowired
	private HomeService homeService;
	@Autowired
	private RoomService roomService;
	@Autowired
	private RoomRepository roomRepository;

	@RequestMapping("/RoomMaster")
	public String RoomMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {

			List<Room> roomList=roomService.AllRoomList();

			List<Home> homeList=homeService.AllHomeList();

			model.addAttribute("homeList",homeList);
			model.addAttribute("roomList",roomList);
			return "RoomMaster";

		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return "Login";
		}
	}

	@ResponseBody
	@RequestMapping("/getRoomListByHomeWise")
	public List<RoomDTO> getSwitchListByRoom(@RequestParam("homeId") String homeId)
	{
		Home home =new Home();
		home.setId(Long.valueOf(homeId));
		List<Room> roomList1=new ArrayList<>();
		List<RoomDTO> roomList=new ArrayList<>(); 
		roomList1=roomRepository.findByHome(home);
		RoomDTO roomDTO=new RoomDTO();
		for(int i=0;i<roomList1.size();i++)
		{
			roomDTO=new RoomDTO();
			roomDTO.setId(roomList1.get(i).getId()+"");
			roomDTO.setRoomName(roomList1.get(i).getRoomName());
			roomDTO.setRoomType(roomList1.get(i).getRoomType());
			roomDTO.setHomeId(roomList1.get(i).getHome().getId()+"");
			roomDTO.setHomeName(roomList1.get(i).getHome().getHomeName());
			//roomDTO.setIotproductNumber(roomList1.get(i).getProductList().get(0).getIotProductNumber()+"");
			roomDTO.setRoomIdentifier(roomList1.get(i).getRoomIdentifier());
			roomList.add(roomDTO);
		}

		return roomList;
	}

}
