package com.gsmarthome.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gsmarthome.dto.SwitchDTO;
import com.gsmarthome.model.Home;
import com.gsmarthome.model.Room;
import com.gsmarthome.model.Switch;
import com.gsmarthome.repository.HomeRepository;
import com.gsmarthome.repository.RoomRepository;
import com.gsmarthome.repository.SwitchRepository;
import com.gsmarthome.services.HomeService;
import com.gsmarthome.services.SwitchService;

@Controller
public class SwitchController {


	@Autowired
	private HomeService homeService;

	@Autowired
	private SwitchService switchService;

	@Autowired
	private HomeRepository homeRepository;

	@Autowired
	private SwitchRepository switchRepository;

	@Autowired
	private RoomRepository roomReposotory;

	@RequestMapping("/SwitchMaster")
	public String SwitchMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<Switch> switchList=switchService.AllSwitchList();

			List<Home> homeList=homeService.AllHomeList();

			model.addAttribute("homeList",homeList);
			model.addAttribute("switchList",switchList);
			return "SwitchMaster";

		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return "Login";
		}
	}

	@ResponseBody
	@RequestMapping("/getSwitchListByHome")
	public List<SwitchDTO> getSwitchListByHome(@RequestParam("homeId") String homeId)
	{
		Home home =new Home();
		home.setId(Long.valueOf(homeId));
		List<Switch> switchList1=new ArrayList<>();
		List<SwitchDTO> switchList=new ArrayList<>(); 
		switchList1=switchRepository.findByIotProductRoomHome(home);
		SwitchDTO switchdto=new SwitchDTO();
		for(int i=0;i<switchList1.size();i++)
		{
			switchdto=new SwitchDTO();
			switchdto.setId(switchList1.get(i).getId()+"");
			switchdto.setSwitchNumber(switchList1.get(i).getSwitchNumber()+"");
			switchdto.setSwitchName(switchList1.get(i).getSwitchName());
			switchdto.setIotproductNumber(switchList1.get(i).getIotProduct().getIotProductNumber()+"");
			switchdto.setIotproductName(switchList1.get(i).getIotProduct().getProductName());
			switchdto.setSwitchType(switchList1.get(i).getSwitchType());
			switchdto.setRoomId(switchList1.get(i).getIotProduct().getRoom().getId()+"");
			switchdto.setRoomName(switchList1.get(i).getIotProduct().getRoom().getRoomName());
			switchdto.setHomeId(switchList1.get(i).getIotProduct().getRoom().getHome().getId()+"");
			switchdto.setHomeName(switchList1.get(i).getIotProduct().getRoom().getHome().getHomeName());
			switchdto.setSwitchStatus(switchList1.get(i).getSwitchStatus());
			switchList.add(switchdto);

		}


		return switchList;
	}

	@ResponseBody
	@RequestMapping("/getRoomListByHome")
	public List<Room> getRoomListByHome(@RequestParam("homeId") String homeId)
	{
		Home home =new Home();
		home.setId(Long.valueOf(homeId));
		List<Room> roomList=new ArrayList<>();
		roomList= roomReposotory.findByHome(home);
		return roomList;
	}

	@ResponseBody
	@RequestMapping("/getSwitchListByRoom")
	public List<SwitchDTO> getSwitchListByRoom(@RequestParam("roomId") String roomId)
	{
		Room room =new Room();
		room.setId(Long.valueOf(roomId));
		List<Switch> switchList1=new ArrayList<>();
		List<SwitchDTO> switchList=new ArrayList<>(); 
		switchList1=switchRepository.findByIotProductRoom(room);
		SwitchDTO switchdto=new SwitchDTO();
		for(int i=0;i<switchList1.size();i++)
		{
			switchdto=new SwitchDTO();
			switchdto.setId(switchList1.get(i).getId()+"");
			switchdto.setSwitchNumber(switchList1.get(i).getSwitchNumber()+"");
			switchdto.setSwitchName(switchList1.get(i).getSwitchName());
			switchdto.setIotproductNumber(switchList1.get(i).getIotProduct().getIotProductNumber()+"");
			switchdto.setIotproductName(switchList1.get(i).getIotProduct().getProductName());
			switchdto.setSwitchType(switchList1.get(i).getSwitchType());
			switchdto.setRoomId(switchList1.get(i).getIotProduct().getRoom().getId()+"");
			switchdto.setRoomName(switchList1.get(i).getIotProduct().getRoom().getRoomName());
			switchdto.setHomeId(switchList1.get(i).getIotProduct().getRoom().getHome().getId()+"");
			switchdto.setHomeName(switchList1.get(i).getIotProduct().getRoom().getHome().getHomeName());
			switchdto.setSwitchStatus(switchList1.get(i).getSwitchStatus());
			switchList.add(switchdto);

		}

		return switchList;
	}

}
