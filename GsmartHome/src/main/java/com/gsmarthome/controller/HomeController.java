package com.gsmarthome.controller;

import java.util.List;
import java.text.DateFormat;  
import java.text.SimpleDateFormat;  
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.amazonaws.util.json.JSONObject;
import com.gsmarthome.commanservices.PostCollection;
import com.gsmarthome.model.DeviceDetails;
import com.gsmarthome.model.Home;
import com.gsmarthome.model.LocalLogs;
import com.gsmarthome.model.Login;
import com.gsmarthome.model.Room;
import com.gsmarthome.services.HomeService;
import com.gsmarthome.services.RoomService;
import com.gsmarthome.repository.*;

import mqttconnection.JdbcConnection;
import mqttconnection.MqttlCentralConnection;

@Controller
public class HomeController {


	@Autowired
	private HomeService homeService;

	@Autowired
	private LocalLogsRepository localLogsRepository;

	@Autowired
	private HomeRepository homeRepository;

	@RequestMapping("/HomeMaster")
	public String HomeMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<Home> homeList=homeService.AllHomeList();

			model.addAttribute("homeList",homeList);
			return "HomeMaster";

		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return "Login";
		}
	}



	@RequestMapping(value = "/LocalHomeDetails")
	public String LocalHomeDetails(@RequestParam("id") long id, Model model)
	{	  
		try {

			DateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy hh:mm:ss");
			
			List<LocalLogs> localLoggsList=localLogsRepository.findByHomeId(id);
			
			Home homeDetails=homeRepository.findById(id);
			for(int i=0;i<localLoggsList.size();i++)
			{
				try {
					if(localLoggsList.get(i).getMessageType()==1)
					{
						localLoggsList.get(i).setMessageTypeString("Reboot Local Home");
					}
					else if(localLoggsList.get(i).getMessageType()==2)
					{
						localLoggsList.get(i).setMessageTypeString("Check local connection");
					}
					else
					{
						localLoggsList.get(i).setMessageTypeString("Deploy war file");
					}
					
					//localLoggsList.get(i).setStringDate(dateFormat.format(localLoggsList.get(i).getCreated_date()));
				
				}catch (Exception e) {
					e.printStackTrace();
					// TODO: handle exception
				}
			}


			model.addAttribute("localLoggsList",localLoggsList);
			model.addAttribute("homeDetails",homeDetails);

			return "LocalHomeDetails";

		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return "Login";
		}
	}

}
