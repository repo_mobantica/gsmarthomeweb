package com.gsmarthome.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;

import com.gsmarthome.model.Login;
import com.gsmarthome.repository.LoginRepository;
import com.gsmarthome.services.DeviceService;
import com.gsmarthome.services.HomeService;
import com.gsmarthome.services.LoginService;
import com.gsmarthome.services.UserService;

@Controller
public class LoginController {


	@Autowired
	LoginRepository loginRepository;

	@Autowired
	private LoginService loginService;

	@Autowired
	private DeviceService deviceService;

	@Autowired
	private UserService userService;

	@Autowired
	private HomeService homeService;

	@RequestMapping("/")
	public String Login()
	{

		return "Login";
	}


	@RequestMapping("/edit")
	public String Edit(@RequestParam int id,  HttpServletRequest req)
	{
		loginService.edit(id);
		return "Home";
	}

	@RequestMapping(value="/login", method=RequestMethod.POST)
	public String Login(@ModelAttribute Login login, Model model, HttpServletRequest req,HttpServletResponse res)
	{
		String username = req.getParameter("username");
		String password = req.getParameter("password");

		try {
			Login loginDetails =loginService.view(username, password);

			if(loginDetails!=null)
			{
				if(loginDetails.getStatus()==1)
				{
					HttpSession session = req.getSession(true);
					session.setAttribute("userName",username);
					session.setAttribute("password",password);
					session.setAttribute("name",loginDetails.getName());
					session.setAttribute("adminRole",loginDetails.getRole());
					session.setAttribute("loginId",loginDetails.getId());

					long totalUsers=userService.countUsers();
					long totalHome=homeService.countHome();
					long openTickets=homeService.countOpenTickets();
					long newEnquiry=homeService.countNewEnquiry();

					model.addAttribute("newEnquiry",newEnquiry);
					model.addAttribute("openTickets",openTickets);
					model.addAttribute("totalHome",totalHome);
					model.addAttribute("totalUsers",totalUsers);
					return "Dashboard";
				}
				else
				{

					model.addAttribute("flag",1);
					return "Login";
				}
			}
			else
			{

				model.addAttribute("flag",2);
				return "Login";
			}
		}catch (Exception e) {
			model.addAttribute("flag",2);
			return "Login";
			// TODO: handle exception
		}

	}

	@RequestMapping("/logout")
	public String logout(HttpServletRequest req,HttpServletResponse res)
	{
		HttpSession session = req.getSession(false);

		session.removeAttribute("userName");
		session.removeAttribute("password");
		session.removeAttribute("name");
		session.removeAttribute("adminRole");

		return "Login";
	}


	@RequestMapping("/login")
	public String login(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
		long totalUsers=userService.countUsers();
		long totalHome=homeService.countHome();
		long openTickets=homeService.countOpenTickets();
		long newEnquiry=homeService.countNewEnquiry();
		
		model.addAttribute("newEnquiry",newEnquiry);
		model.addAttribute("openTickets",openTickets);
		model.addAttribute("totalHome",totalHome);
		model.addAttribute("totalUsers",totalUsers);
		return "Dashboard";
		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return "Login";
		}
	}
	
}
