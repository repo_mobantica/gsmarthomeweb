package com.gsmarthome.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.gsmarthome.model.UserDetails;
import com.gsmarthome.services.UserService;

@Controller
public class UserController {


	@Autowired
	private UserService userService;



	@RequestMapping("/UserMaster")
	public String UserMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {

			List<UserDetails> userList=userService.AllUserList();
			model.addAttribute("userList",userList);
			return "UserMaster";

		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return "Login";
		}
	}


}
