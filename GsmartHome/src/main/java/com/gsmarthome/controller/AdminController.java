package com.gsmarthome.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gsmarthome.model.DeviceDetails;
import com.gsmarthome.model.Login;
import com.gsmarthome.repository.AdminRepository;
import com.gsmarthome.services.AdminService;

@Controller
public class AdminController {

	@Autowired
	AdminService adminService;

	@Autowired
	AdminRepository adminRepository;

	@RequestMapping("/AdminMaster")
	public String DeviceMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<Login> adminList=adminService.AllAdminList();

			for(int i=0;i<adminList.size();i++)
			{
				if(adminList.get(i).getStatus()==1)
				{
					adminList.get(i).setActiveStatus("Active");
				}
				else
				{
					adminList.get(i).setActiveStatus("InActive");
				}
			}

			model.addAttribute("adminList",adminList);
			return "AdminMaster";
		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return "Login";
		}
	}

	@RequestMapping("/AddAdmin")
	public String AddAdmin(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			return "AddAdmin";
		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return "Login";
		}
	}



	@RequestMapping(value = "/AddAdmin", method = RequestMethod.POST)
	public String AddAdmin(@ModelAttribute Login login, Model model)
	{
		try {
			adminService.savenewAdmin(login);

			List<Login> adminList=adminService.AllAdminList();

			for(int i=0;i<adminList.size();i++)
			{
				if(adminList.get(i).getStatus()==1)
				{
					adminList.get(i).setActiveStatus("Active");
				}
				else
				{
					adminList.get(i).setActiveStatus("InActive");
				}
			}

			model.addAttribute("adminList",adminList);
			return "AdminMaster";
		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return "Login";
		}
	}

	@RequestMapping("/EditAdmin")
	public String EditAdmin(@RequestParam("id") int id, ModelMap model)
	{
		try {
			Login adminDetails=adminRepository.findById(id);

			model.addAttribute("adminDetails", adminDetails);
			return "EditAdmin";
		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return "Login";
		}
	}


	@ResponseBody
	@RequestMapping(value="/checkUserNameAlreadyExit",method=RequestMethod.POST)
	public Login checkUserNameAlreadyExit(@RequestParam("username") String username)
	{
		Login login=adminRepository.findByUsername(username);

		return login;
	}


	@RequestMapping("/profile")
	public String profile(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		HttpSession session = req.getSession(true);
		if(session.getAttribute("loginId")!=null)
		{
			int id= (int)session.getAttribute("loginId");
			Login adminDetails=adminRepository.findById(id);

			model.addAttribute("adminDetails", adminDetails);
			return "Profile";
		}
		else
		{
			return "Login";
		}
	}


}
