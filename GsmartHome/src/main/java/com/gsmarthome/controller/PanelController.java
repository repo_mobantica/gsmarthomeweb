package com.gsmarthome.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gsmarthome.model.Panel;
import com.gsmarthome.services.PanelService;

@Controller
public class PanelController {


	@Autowired
	private PanelService panelService;

	@RequestMapping("/PanelMaster")
	public String PanelMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {

			List<Panel> panelList=panelService.AllHomeList();

			model.addAttribute("panelList",panelList);
			return "PanelMaster";

		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return "Login";
		}
	}

}
