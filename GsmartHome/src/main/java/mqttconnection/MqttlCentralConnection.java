package mqttconnection;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;

import javax.swing.SwingUtilities;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class MqttlCentralConnection
implements MqttCallback
{
	String subscribePanel;
	private String localClient = "119";
	private String localInternet = "119cdcd";
	public static MqttClient mqttConnectionLocal = null;
	public static MqttClient mqttConnectionInternet = null;

	private static String localBroker = "tcp://localhost:1883";

	private static String internetBroker = "tcp://103.12.211.52:1883";
	//private static String internetBroker = "tcp://192.168.0.18:1883";

	private static MqttlCentralConnection currObj = null;

	public MqttlCentralConnection() {}

	public MqttlCentralConnection(String csubscribePanel)
	{
		subscribePanel = csubscribePanel;
	}


	public MqttClient getInternetMqttConnection()
	{

		if ((mqttConnectionInternet != null) && (mqttConnectionInternet.isConnected())) {
			System.out.println("reusing connection...for AWS");
			return mqttConnectionInternet;
		}
		try {
			MemoryPersistence persistence = new MemoryPersistence();
			mqttConnectionInternet = new MqttClient(internetBroker, localInternet, persistence);
			MqttConnectOptions connOpts = new MqttConnectOptions();
			connOpts.setCleanSession(true);

			mqttConnectionInternet.connect(connOpts);
			//mqttConnectionInternet.subscribe(topic);
			mqttConnectionInternet.subscribe("GenieHomeId");
			// mqttConnectionInternet.subscribe("shareControl");
			mqttConnectionInternet.setCallback(this);

			return mqttConnectionInternet;
		} catch (MqttException exception) {
			System.out.println("unable to connect to AWS hub");
			System.out.println("trying...");

			getObj().getInternetMqttConnection();
		}
		return mqttConnectionInternet;
	}



	public static MqttlCentralConnection getObj()
	{
		if (currObj == null) {
			currObj = new MqttlCentralConnection();
			return currObj;
		}
		return currObj;
	}



	public void connectionLost(Throwable arg0) {}


	public void deliveryComplete(IMqttDeliveryToken arg0) {}



	public void messageArrived(String sendTopic, MqttMessage mqttmessage)
			throws Exception
	{
		System.out.println("mqttmessage = "+mqttmessage);
		//if(!mqttmessage.toString().equalsIgnoreCase(""))
		{
			JdbcConnection jdbcConnection=new JdbcConnection();

			jdbcConnection.saveOpration(mqttmessage.toString());
		}
	}
}
