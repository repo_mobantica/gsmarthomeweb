<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="s"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Device Master</title>

<!-- Bootstrap Core CSS -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="resources/css/metisMenu.min.css" rel="stylesheet">

<!-- DataTables CSS -->
<link href="resources/css/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="resources/css/dataTables/dataTables.responsive.css"
	rel="stylesheet">

<!-- Custom CSS -->
<link href="resources/css/startmin.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="resources/css/font-awesome.min.css" rel="stylesheet"
	type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>
<body onLoad="init()">

	<%
		response.setHeader("Cache-Control", "no-cache,no-store,must-revalidate");//HTTP 1.1
		response.setHeader("Pragma", "no-cache"); //HTTP 1.0
		response.setDateHeader("Expires", 0);

		if (session != null) {
			if (session.getAttribute("userName") == null || session.getAttribute("password") == null) {
				response.sendRedirect("");
			}
		}
	%>
	<div id="wrapper">

		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

			<%@ include file="header.jsp"%>

			<%@ include file="menu.jsp"%>

		</nav>

		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<h3 class="page-header">Admin List</h3>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel panel-green"></div>

							<div class="panel-body">
								<div class="row">

									<div class="col-lg-6"></div>
									<div class="col-lg-6">

										<a href="AddAdmin">
											<button type="submit" class="btn btn-info">Add New
												Admin</button>
										</a>

										<!-- 
                                                <button type="reset" class="btn btn-default">Reset Button</button>
                                                 -->
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>

				<form name="adminform">


					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-body">


									<div class="table-responsive" id="nonedit">
										<table class="table table-striped table-bordered table-hover"
											id="dataTables-example">
											<thead>
												<tr>
													<th>Id</th>
													<th>Name</th>
													<th>User Name</th>
													<th>Status</th>
												</tr>
											</thead>

											<tbody>
												<s:forEach items="${adminList}" var="adminList"
													varStatus="loopStatus">
													<tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
														<td>${adminList.id}</td>
														<td>${adminList.name}</td>
														<td>${adminList.username}</td>
														<td>${adminList.activeStatus}</td>

													</tr>
												</s:forEach>

											</tbody>

										</table>


									</div>

									<div class="table-responsive" id="edit">
										<table class="table table-striped table-bordered table-hover"
											id="dataTables-example">
											<thead>
												<tr>
													<th>Id</th>
													<th>Name</th>
													<th>User Name</th>
													<th>Status</th>
													<th>Action</th>
												</tr>
											</thead>

											<tbody>
												<s:forEach items="${adminList}" var="adminList"
													varStatus="loopStatus">
													<tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
														<td>${adminList.id}</td>
														<td>${adminList.name}</td>
														<td>${adminList.username}</td>
														<td>${adminList.activeStatus}</td>

														<td><a
															href="${pageContext.request.contextPath}/EditAdmin?id=${adminList.id}"
															class="btn btn-info btn-sm" data-toggle="tooltip"
															title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>


													</tr>
												</s:forEach>

											</tbody>

										</table>


									</div>
								</div>

								<input type="hidden" class="form-control" id="adminRole"
									name="adminRole" value="<%=session.getAttribute("adminRole")%>">
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>

	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script src="resources/js/jquery.min.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="resources/js/bootstrap.min.js"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script src="resources/js/metisMenu.min.js"></script>

	<!-- DataTables JavaScript -->
	<script src="resources/js/dataTables/jquery.dataTables.min.js"></script>
	<script src="resources/js/dataTables/dataTables.bootstrap.min.js"></script>

	<!-- Custom Theme JavaScript -->
	<script src="resources/js/startmin.js"></script>

	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
	<script>
		function init() {
			var adminRole=document.adminform.adminRole.value;
			if(adminRole=="Normal_Admin")
				{
				document.getElementById("edit").style.display = "none";
				document.getElementById("nonedit").style.display = "block";
				//document.getElementById("edit").disabled = true;
				}
			else
				{
				document.getElementById("edit").style.display = "block";
				document.getElementById("nonedit").style.display = "none";
				}
		}
		$(document).ready(function() {
			$('#dataTables-example').DataTable({
				responsive : true
			});
		});
	
		
	</script>

</body>
</html>
