<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="s"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=admin-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Edit Details</title>

<!-- Bootstrap Core CSS -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="resources/css/metisMenu.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="resources/css/startmin.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="resources/css/font-awesome.min.css" rel="stylesheet"
	type="text/css">

<!-- DataTables CSS -->
<link href="resources/css/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="resources/css/dataTables/dataTables.responsive.css"
	rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>
<body onLoad="init()">

	<%
		response.setHeader("Cache-Control", "no-cache,no-store,must-revalidate");//HTTP 1.1
		response.setHeader("Pragma", "no-cache"); //HTTP 1.0
		response.setDateHeader("Expires", 0);

		if (session != null) {
			if (session.getAttribute("userName") == null || session.getAttribute("password") == null) {
				response.sendRedirect("");
			}
		}
	%>
	<div id="wrapper">

		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

			<%@ include file="header.jsp"%>

			<%@ include file="menu.jsp"%>

		</nav>

		<div id="page-wrapper">
			<div class="container-fluid">

				<form name="enquiryform"
					action="${pageContext.request.contextPath}/EditEnquiry"
					onSubmit="return validate()" method="post">

					<div class="panel panel-default">
						<div class="row">

							<div class="col-lg-12">
								<h3>Details</h3>
							</div>
							<div class="col-lg-12">

								<div class="panel panel-green"></div>

								<div class="panel-body">
									<div class="row">

										<div class="col-lg-12">
											<table>

												<thead>
													<tr>
														<th style="width: 150px"><label>Token Id</label></th>
														<th style="width: 150px"><label>Home Id</label></th>
														<th style="width: 200px"><label>Name</label></th>
														<th style="width: 150px"><label>Type</label></th>
														<th><label>Device</label></th>
													</tr>
												</thead>

												<tbody>

													<tr>
														<th>
															<p class="help-block">${enquiryDetails.id}</p>
														</th>
														<th>
															<p class="help-block">${enquiryDetails.userDetails.home.id}</p>
														</th>
														<th>
															<p class="help-block">${enquiryDetails.userDetails.firstName}
																${enquiryDetails.userDetails.lastName}</p>
														</th>
														<th>
															<p class="help-block">${enquiryDetails.msgType}</p>
														</th>

														<th>
															<p class="help-block">${enquiryDetails.deviceId}</p>
														</th>
													</tr>
												</tbody>
											</table>

										</div>

									</div>
								</div>

								<div class="panel-body">
									<div class="row">

										<div class="col-lg-12">
											<div class="form-group">
												<label>Customer Message</label>
												<p class="help-block">${enquiryDetails.message}</p>
											</div>
										</div>

									</div>
								</div>

								<div class="panel panel-green"></div>

								<div class="panel-body">
									<div class="row">

										<div class="col-lg-4">

											<div class="form-group">
												<label>Issue Action</label> <input class="form-control"
													id="issue_Action" name="issue_Action"
													placeholder="Issue Action"> <span
													id="issue_ActionSpan" style="color: #FF0000"></span>
											</div>
										</div>

										<div class="col-lg-3">
											<div class="form-group">
												<label>Status</label> <select class="form-control"
													id="status" name="status">
													<option value="Default">Select Status -</option>
													<option value="2">In-Progress</option>
													<option value="3">Complete</option>

												</select> <span id="statusSpan" style="color: #FF0000"></span>
											</div>
										</div>


										<input type="hidden" class="form-control" id="loginId"
											name="loginId" value="<%=session.getAttribute("loginId")%>">

										<input type="hidden" class="form-control" id="enquiryId"
											name="enquiryId" value="${enquiryDetails.id}"> <input
											type="hidden" class="form-control" id="userid" name="userid"
											value="${enquiryDetails.userDetails.id}"> <input
											type="hidden" class="form-control" id="priviousStatus"
											name="priviousStatus" value="${enquiryDetails.status}">

										<input type="hidden" class="form-control" id="priviousMsgType"
											name="priviousMsgType" value="${enquiryDetails.messageType}">

									</div>
								</div>

								<div class="panel-body">
									<div class="row">

										<div class="col-lg-3"></div>

										<div class="col-lg-3">
											<div class="col-lg-3">

												<a href="#" id="abc"><button type="button"
														class="btn btn-block btn-primary" style="width: 80px">Back</button></a>
											</div>
										</div>

										<div class="col-lg-3">
											<button type="reset" class="btn btn-default" value="reset"
												style="width: 80px">Reset</button>
										</div>

										<div class="col-lg-3">
											<button type="submit" class="btn btn-success"
												style="width: 80px">Save</button>
										</div>

									</div>
								</div>


								<div class="panel-body">
									<div class="row">

										<div class="panel panel-green"></div>
										<!-- /.panel-heading -->

										<div class="col-lg-9">

											<h3 class="page-header">Details</h3>
											<div class="panel-body">
												<div class="table-responsive">
													<table
														class="table table-striped table-bordered table-hover"
														id="dataTables-example">
														<thead>
															<tr>
																<th>Issue Action</th>
																<th>Date</th>
																<th>Status</th>
																<th>Login Name</th>
															</tr>
														</thead>

														<tbody>
															<s:forEach items="${enquiryhistoriesList}"
																var="enquiryhistoriesList" varStatus="loopStatus">
																<tr
																	class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
																	<td>${enquiryhistoriesList.issue_Action}</td>
																	<td>${enquiryhistoriesList.created_date}</td>
																	<td>${enquiryhistoriesList.stringStatus}</td>
																	<td>${enquiryhistoriesList.loginDetails.name}</td>

																</tr>
															</s:forEach>

														</tbody>

													</table>
												</div>
											</div>
										</div>

									</div>
								</div>

							</div>
						</div>
					</div>

				</form>

			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script src="resources/js/jquery.min.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="resources/js/bootstrap.min.js"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script src="resources/js/metisMenu.min.js"></script>

	<!-- Custom Theme JavaScript -->
	<script src="resources/js/startmin.js"></script>

	<!-- DataTables JavaScript -->
	<script src="resources/js/dataTables/jquery.dataTables.min.js"></script>
	<script src="resources/js/dataTables/dataTables.bootstrap.min.js"></script>

	<script>
		function init() {

			var priviousStatus = $('#priviousStatus').val();
			var priviousMsgType = $('#priviousMsgType').val();
			var url = "";

			if (priviousMsgType == 1) {
				url = "${pageContext.request.contextPath}/FeedbackMaster";
			}
			if (priviousMsgType == 2) {
				if (priviousStatus == 1) {
					url = "${pageContext.request.contextPath}/ComplaintMaster";
				}
				if (priviousStatus == 2) {
					url = "${pageContext.request.contextPath}/InprogressComplaintMaster";
				}
				if (priviousStatus == 3) {
					url = "${pageContext.request.contextPath}/CompletedComplaintMaster";
				}

			}
			if (priviousMsgType == 3) {
				url = "${pageContext.request.contextPath}/EnquiryMaster";
			}

			$(document).ready(function() {
				$("#abc").attr("href", url);
			});

			clear();
			document.enquiryform.issue_Action.focus();

		}
		function clear() {

			$('#issue_ActionSpan').html('');
			$('#statusSpan').html('');
		}
		function validate() {
			clear();
			if (document.enquiryform.issue_Action.value == "") {
				$('#issue_ActionSpan').html('Please, enter  Action ..!');
				document.enquiryform.issue_Action.focus();
				return false;
			} else if (document.enquiryform.issue_Action.value.match(/^[\s]+$/)) {
				$('#issue_ActionSpan').html('Please, enter Action..!');
				document.enquiryform.issue_Action.value = "";
				document.enquiryform.issue_Action.focus();
				return false;
			}

			if (document.enquiryform.status.value == "Default") {
				$('#statusSpan').html('Please, select Status..!');
				document.enquiryform.status.focus();
				return false;
			}

		}
	</script>
	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
	<%-- <script>
		$(document).ready(function() {
			$('#dataTables-example').DataTable({
				responsive : true
			});
		});
	</script>
 --%>
</body>
</html>
