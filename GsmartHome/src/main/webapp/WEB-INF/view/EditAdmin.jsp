<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=admin-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Add Admin</title>

<!-- Bootstrap Core CSS -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="resources/css/metisMenu.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="resources/css/startmin.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="resources/css/font-awesome.min.css" rel="stylesheet"
	type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>
<body onLoad="init()">

	<%
		response.setHeader("Cache-Control", "no-cache,no-store,must-revalidate");//HTTP 1.1
		response.setHeader("Pragma", "no-cache"); //HTTP 1.0
		response.setDateHeader("Expires", 0);

		if (session != null) {
			if (session.getAttribute("userName") == null || session.getAttribute("password") == null) {
				response.sendRedirect("");
			}
		}
	%>
	<div id="wrapper">

		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

			<%@ include file="header.jsp"%>

			<%@ include file="menu.jsp"%>

		</nav>

		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<h3 class="page-header">Add Admin</h3>
					</div>
				</div>
				<!-- /.row -->

				<form name="adminform"
					action="${pageContext.request.contextPath}/AddAdmin"
					onSubmit="return validate()" method="post">

					<div class="panel panel-default">
						<div class="row">
							<div class="col-lg-12">

                            <div class="panel panel-green"></div>

								<div class="panel-body">
									<div class="row">

										<div class="col-lg-3">
											<div class="form-group">
												<label>Name</label> <input type="text" class="form-control"
													id="name" name="name" value="${adminDetails.name}">
											</div>
										</div>

										<div class="col-lg-3">
											<div class="form-group">
												<label>User Name</label> <input class="form-control"
													id="username" name="username" placeholder="Username"
													value="${adminDetails.username}"
													onchange="checkUserNameAlreadyExit(this.value)"> <span
													id="usernameSpan" style="color: #FF0000"></span>
											</div>
										</div>

										<div class="col-lg-3">
											<div class="form-group">
												<label>Password</label> <input class="form-control"
													id="password" name="password" type="password"
													value="${adminDetails.password}"> <span
													id="passwordSpan" style="color: #FF0000"></span>
											</div>
										</div>

										<div class="col-lg-3">
											<div class="form-group">
												<label>Role</label> <select class="form-control" id="role"
													name="role">

													<c:choose>
														<c:when test="${adminDetails.role eq 'Super_Admin'}">
															<option selected="selected" value="Super_Admin">Super
																Admin</option>
														</c:when>
														<c:otherwise>
															<option value="Super_Admin">Super Admin</option>
														</c:otherwise>
													</c:choose>

													<c:choose>
														<c:when test="${adminDetails.role eq 'Normal_Admin'}">
															<option selected="selected" value="Normal_Admin">Normal
																Admin</option>
														</c:when>
														<c:otherwise>
															<option value="Normal_Admin">Normal Admin</option>
														</c:otherwise>
													</c:choose>

												</select> <span id="roleSpan" style="color: #FF0000"></span>
											</div>
										</div>
									</div>
								</div>
								<div class="panel-body">
									<div class="row">

										<div class="col-lg-3">

											<div class="form-group">
												<label>Login Type</label> <input class="form-control"
													id="login_type" name="login_type" placeholder="Login Type"
													value="${adminDetails.login_type}"> <span
													id="login_typeSpan" style="color: #FF0000"></span>
											</div>
										</div>

										<div class="col-lg-3">
											<div class="form-group">
												<label>Status</label> <select class="form-control"
													id="status" name="status">

													<c:choose>
														<c:when test="${adminDetails.status eq '1'}">
															<option selected="selected" value="1">Active</option>
														</c:when>
														<c:otherwise>
															<option value="1">Active</option>
														</c:otherwise>
													</c:choose>

													<c:choose>
														<c:when test="${adminDetails.status eq '0'}">
															<option selected="selected" value="0">In-Active</option>
														</c:when>
														<c:otherwise>
															<option value="0">In-Active</option>
														</c:otherwise>
													</c:choose>

												</select> <span id="statusSpan" style="color: #FF0000"></span>
											</div>
										</div>

										<%-- 
										<input type="hidden" class="form-control" id="password"
											name="password" value="${adminDetails.password}">
											 --%>
										<input type="hidden" class="form-control" id="id" name="id"
											value="${adminDetails.id}">

									</div>
								</div>

								<div class="panel-body">
									<div class="row">

										<div class="col-lg-3"></div>

										<div class="col-lg-3">
											<div class="col-lg-3">
												<a href="AdminMaster"><button type="button"
														class="btn btn-block btn-primary" value="Back"
														style="width: 80px">Back</button></a>
											</div>
										</div>

										<div class="col-lg-3">


											<button type="reset" class="btn btn-default" value="reset"
												style="width: 80px">Reset</button>
										</div>

										<div class="col-lg-3">
											<button type="submit" class="btn btn-success"
												style="width: 80px">Save</button>
										</div>

									</div>
								</div>


							</div>
						</div>
					</div>


				</form>



				<!-- /.row -->
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script src="resources/js/jquery.min.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="resources/js/bootstrap.min.js"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script src="resources/js/metisMenu.min.js"></script>

	<!-- Custom Theme JavaScript -->
	<script src="resources/js/startmin.js"></script>

	<script>
		function init() {

			clear();
			document.adminform.name.focus();

		}
		function clear()
		{

			$('#nameSpan').html('');
			$('#usernameSpan').html('');
			$('#passwordSpan').html('');
			$('#login_typeSpan').html('');
			$('#nameSpan').html('');
		}
		function validate() {
			clear();
			if (document.adminform.name.value == "") {
				$('#nameSpan').html('Please, enter  Name..!');
				document.adminform.name.focus();
				return false;
			} else if (document.adminform.name.value.match(/^[\s]+$/)) {
				$('#nameSpan').html('Please, enter Name..!');
				document.adminform.name.value = "";
				document.adminform.name.focus();
				return false;
			}
			
			//for user name
			if (document.adminform.username.value == "") {
				$('#usernameSpan').html('Please, enter  user Name..!');
				document.adminform.username.focus();
				return false;
			} else if (document.adminform.username.value.match(/^[\s]+$/)) {
				$('#usernameSpan').html('Please, enter User Name..!');
				document.adminform.username.value = "";
				document.adminform.username.focus();
				return false;
			}


			//for password
			if (document.adminform.password.value == "") {
				$('#passwordSpan').html('Please, enter  user Name..!');
				document.adminform.username.focus();
				return false;
			} else if (document.adminform.username.value.match(/^[\s]+$/)) {
				$('#passwordSpan').html('Please, enter User Name..!');
				document.adminform.username.value = "";
				document.adminform.username.focus();
				return false;
			}
			if(document.adminform.role.value=="Default")
			{
				$('#roleSpan').html('Please, select Role..!');
				document.adminform.role.focus();
				return false;
			}
			
			//for login_type
			if (document.adminform.login_type.value == "") {
				$('#login_typeSpan').html('Please, enter login_type ..!');
				document.adminform.login_type.focus();
				return false;
			} else if (document.adminform.login_type.value.match(/^[\s]+$/)) {
				$('#login_typeSpan').html('Please, enter login_type ..!');
				document.adminform.login_type.value = "";
				document.adminform.login_type.focus();
				return false;
			}

		}
		

		function checkUserNameAlreadyExit() 
		{

			$('#usernameSpan').html('');
			var username = $('#username').val();

					$.ajax({

						url : '${pageContext.request.contextPath}/checkUserNameAlreadyExit',
						type : 'Post',
						data : {
							username : username
						},
						dataType : 'json',
						success : function(result) {
							if (result) {
								$('#usernameSpan').html(
										' This username already present..!');
								document.adminform.username.value = "";
								document.adminform.username.focus();
							} else {
								alert("failure111");
								//$("#ajax_div").hide();
							}
						}
					});

		}
	</script>
</body>
</html>
