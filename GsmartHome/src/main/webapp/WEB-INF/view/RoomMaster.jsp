<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="s"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Room Master</title>

<!-- Bootstrap Core CSS -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="resources/css/metisMenu.min.css" rel="stylesheet">

<!-- DataTables CSS -->
<link href="resources/css/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="resources/css/dataTables/dataTables.responsive.css"
	rel="stylesheet">

<!-- Custom CSS -->
<link href="resources/css/startmin.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="resources/css/font-awesome.min.css" rel="stylesheet"
	type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>
<body>

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("userName") == null || session.getAttribute("password") == null ) 
    			{
    				response.sendRedirect("");
    			} 
    		}
	%>
	<div id="wrapper">

		<!-- Navigation -->
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

			<%@ include file="header.jsp"%>

			<%@ include file="menu.jsp"%>
		</nav>

		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<h3 class="page-header">Room List</h3>
					</div>
				</div>


				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">

							<div class="panel panel-green"></div>
							<div class="panel-body">
								<div class="row">

									<div class="col-lg-3">
										<label>Home</label> <select class="form-control" id="homeId"
											name="homeId" onchange="getRoomListByHome(this.value)">
											<option selected="selected" value="Default">-Select
												Home-</option>
											<s:forEach var="homeList" items="${homeList}">
												<option value="${homeList.id}">${homeList.homeName}</option>
											</s:forEach>
										</select>

									</div>

								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<!-- /.panel-heading -->
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover"
										id="roomListTable">
										<thead>
											<tr>
												<th>Id</th>
												<th>Room Name</th>
												<th>Type</th>
												<th>Room Identifier</th>
												<th>Home id</th>
												<th>Home Name</th>
											</tr>
										</thead>

										<tbody>
											<s:forEach items="${roomList}" var="roomList"
												varStatus="loopStatus">
												<tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
													<td>${roomList.id}</td>
													<td>${roomList.roomName}</td>
													<td>${roomList.roomType}</td>
													<td>${roomList.roomIdentifier}</td>
													<td>${roomList.home.id}</td>
													<td>${roomList.home.homeName}</td>
													<%-- 
													<td>${roomList.bankifscCode}</td>
													<td><a
														href="${pageContext.request.contextPath}/EditBank?bankId=${roomList.bankId}"
														class="btn btn-info btn-sm" data-toggle="tooltip"
														title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td> --%>
												</tr>
											</s:forEach>

										</tbody>

									</table>
								</div>
							</div>
						</div>
					</div>
				</div>


			</div>
		</div>

	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script src="resources/js/jquery.min.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="resources/js/bootstrap.min.js"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script src="resources/js/metisMenu.min.js"></script>

	<!-- DataTables JavaScript -->
	<script src="resources/js/dataTables/jquery.dataTables.min.js"></script>
	<script src="resources/js/dataTables/dataTables.bootstrap.min.js"></script>

	<!-- Custom Theme JavaScript -->
	<script src="resources/js/startmin.js"></script>

	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
	<script>

    	function getRoomListByHome()
    	{

    		 var homeId = $('#homeId').val();
    		 
    		 $("#roomListTable tr").detach();
    	
    	 $.ajax({

    		 url : '${pageContext.request.contextPath}/getRoomListByHomeWise',
    		type : 'Post',
    		data : { homeId : homeId},
    		dataType : 'json',
    		success : function(result)
    				  {
    						if (result) 
    						{
    						$('#roomListTable')
    										.append(
    												'<tr><th>Id</th><th>Room Name</th><th>Type</th><th>Room Identifier</th><th>Home Id</th><th>Home Name</th></tr>');

    								for (var i = 0; i < result.length; i++) {

    										$('#roomListTable')
    												.append(
    														'<tr><td>'
    																+ result[i].id
    																+ '</td><td>'
    																+ result[i].roomName
    																+ '</td><td>'
    																+ result[i].roomType
    																+ '</td><td>'
    																+ result[i].roomIdentifier
    																+ '</td><td>'
    																+ result[i].homeId
    																+ '</td><td>'
    																+ result[i].homeName
    																+ '</td></tr>');
    								}
    							} else {
    								alert("failure111");
    							}

    						}
    					});

    		}
    	
    	
            $(document).ready(function() {
                $('#roomListTable').DataTable({
                        responsive: true
                });
            });
        </script>

</body>
</html>
