  <div class="navbar-header">
                    <a class="navbar-brand" href="Dashboard">GSMART HOME</a>
                </div>

                <ul class="nav navbar-right navbar-top-links">
                 
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i> ${name} <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu dropdown-user"> 
                           <li><a href="profile"><i class="fa fa-user fa-fw"></i> User Profile</a>
                            </li>
                           <!-- 
                            <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                            </li> -->
                            <li class="divider"></li>
                            <li><a href="logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                            </li>
                        </ul>
                    </li>
                </ul>
              