<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="s"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Home Master</title>

<!-- Bootstrap Core CSS -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="resources/css/metisMenu.min.css" rel="stylesheet">

<!-- DataTables CSS -->
<link href="resources/css/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="resources/css/dataTables/dataTables.responsive.css"
	rel="stylesheet">

<!-- Custom CSS -->
<link href="resources/css/startmin.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="resources/css/font-awesome.min.css" rel="stylesheet"
	type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>
<body>

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("userName") == null || session.getAttribute("password") == null ) 
    			{
    				response.sendRedirect("");
    			} 
    		}
	%>
	<div id="wrapper">

		<!-- Navigation -->
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

			<%@ include file="header.jsp"%>

			<%@ include file="menu.jsp"%>
		</nav>

		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<h3 class="page-header">Home List</h3>
					</div>
				</div>
				<!-- /.row -->
				<div class="row">

					<div class="col-lg-12">
						<div class="col-lg-9">
						</div>
						<div class="col-lg-3">
							<button type="button" onclick="connectMqqt(this.value)"
								class="btn btn-success" >Mqqt Connect</button>
						</div>
					</div>

					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel panel-green"></div>

							<!-- /.panel-heading -->
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover"
										id="dataTables-example">
										<thead>
											<tr>
												<th>Id</th>
												<th>Home Name</th>
												<th style="width:70px">Local Connection</th>
												<th style="width:70px">Reboot Home</th>
												<th style="width:70px">Deploy War File Home</th>
												<th style="width:70px">Local Loggs</th>
											</tr>
										</thead>

										<tbody>
											<s:forEach items="${homeList}" var="homeList"
												varStatus="loopStatus">
												<tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
													<td>${homeList.id}</td>
													<td>${homeList.homeName}</td>
												
													<td>
						                      			  <a onclick="return checkLocalConnection('${homeList.id}')" class="btn btn-info btn-sm" data-toggle="tooltip" title="check Local Connection"><i class="fa fa-check"></i></a>
														</td> 
													<td>
						                       			 <a onclick="return rebootLocalHome('${homeList.id}')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="reboot Local Home"><i class="fa fa-server"></i></a>
														</td> 
													<td>
						                      			  <a onclick="return deployWarFileInLocalHome('${homeList.id}')" class="btn btn-warning btn-sm" data-toggle="tooltip" title="deployWar File In Local Home"><i class="glyphicon glyphicon-edit"></i></a>
													
														</td> 
														
													<td><a
														href="${pageContext.request.contextPath}/LocalHomeDetails?id=${homeList.id}"
														class="btn btn-success btn-sm" data-toggle="tooltip"
														title="View"><i class="fa fa-eye"></i></a></td>
												</tr>
											</s:forEach>

										</tbody>

									</table>
								</div>
							</div>
						</div>
					</div>
				</div>


			</div>
		</div>

	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script src="resources/js/jquery.min.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="resources/js/bootstrap.min.js"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script src="resources/js/metisMenu.min.js"></script>

	<!-- DataTables JavaScript -->
	<script src="resources/js/dataTables/jquery.dataTables.min.js"></script>
	<script src="resources/js/dataTables/dataTables.bootstrap.min.js"></script>

	<!-- Custom Theme JavaScript -->
	<script src="resources/js/startmin.js"></script>

	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
	<script>
            $(document).ready(function() {
                $('#dataTables-example').DataTable({
                        responsive: true
                });
            });
        </script>

<script>
function checkLocalConnection(id)
{
	$.ajax({

		url : '${pageContext.request.contextPath}/checkLocalConnection',
		type : 'Post',
		data : { id : id},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
							
						} 
						else
						{
							alert("failure111");
						}
						
					}
		});
}

function rebootLocalHome(id)
{
	$.ajax({

		url : '${pageContext.request.contextPath}/rebootLocalHome',
		type : 'Post',
		data : { id : id},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
							
						} 
						else
						{
							alert("failure111");
						}
						
					}
		});
}

function deployWarFileInLocalHome(id)
{
	$.ajax({

		url : '${pageContext.request.contextPath}/deployWarFileInLocalHome',
		type : 'Post',
		data : { id : id},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
							
						} 
						else
						{
							alert("failure111");
						}
						
					}
		});
}
function connectMqqt()
{
	
	$.ajax({

		url : '${pageContext.request.contextPath}/connectMqttConnection',
		type : 'Post',
		data : {
		},
		dataType : 'json',
		success : function(result) {
			if (result) {
			} else {
				alert("failure111");
				//$("#ajax_div").hide();
			}
		}
	});

}
</script>


</body>
</html>
