<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Login</title>

<!-- Bootstrap Core CSS -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="resources/css/metisMenu.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="resources/css/startmin.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="resources/css/font-awesome.min.css" rel="stylesheet"
	type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
<style>
body {
	background-image: url("/resources/dist/img/logo4.jpg");
	height: 50%;
	width: 100%;
	background-size: cover;
	background-repeat: no-repeat;
}
</style>
</head>
<body onload="init()">

	<br />
	<br />
	<br />
	<br />
	<br />
	<div class="container">
		<div class="row">

			<div class="col-md-4 col-md-offset-4">
				<div class="login-panel panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Please Sign In</h3>
					</div>
					<div class="panel-body">

						<span id="flagStatusSpan" style="color: #FF0000"></span>

						<form name="loginform"
							action="${pageContext.request.contextPath}/login"
							onsubmit="return validate()" method="post">
							<fieldset>

								<div class="form-group">
									<input class="form-control" placeholder="E-mail"
										name="username" type="username" autofocus>
										<span id="usernameSpan" style="color: #FF0000"></span>
								</div>
								<div class="form-group">
									<input class="form-control" placeholder="Password"
										name="password" type="password" value="">
										<span id="passwordSpan" style="color: #FF0000"></span>
								</div>
								<div class="checkbox">
									<label> <!--   <input name="remember" type="checkbox" value="Remember Me">Remember Me -->
									</label>
								</div>
								<!-- Change this to a button or input when using this as a form -->

								<!--   <a href="index.html" class="btn btn-lg btn-success btn-block">Login</a> -->
								<input type="submit" name="submit" id="submit"
									class="btn btn-lg btn-success btn-block" value="Login">
							</fieldset>


							<input type="hidden" class="form-control" id="flag" name="flag"
								value="${flag}">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- jQuery -->
	<script src="resources/js/jquery.min.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="resources/js/bootstrap.min.js"></script>

	<!-- Metis Menu Plugin JavaScript -->

	<script src="resources/js/metisMenu.min.js"></script>

	<!-- Custom Theme JavaScript -->
	<script src="resources/js/startmin.js"></script>

	<script>
		function init() {
			
			var flag=document.loginform.flag.value;
			if(flag==1){
				$('#flagStatusSpan').html('your account is In_Active Please contact super Admin');
				}
			else if(flag==2)
				{
				$('#flagStatusSpan').html('username or password are incoreect please reenter');
				}
			else
				{
				$('#flagStatusSpan').html('');
				}

		}

		function validate() {

			$('#usernameSpan').html('');
			$('#passwordSpan').html('');
			
			if (document.loginform.username.value == "") {
				$('#usernameSpan').html('Please, enter User Name..!');
				document.loginform.username.focus();
				return false;
			} else if (document.loginform.username.value.match(/^[\s]+$/)) {
				$('#usernameSpan').html('Please, enter user Name..!');
				document.loginform.username.value = "";
				document.loginform.username.focus();
				return false;
			}
			
			if (document.loginform.password.value == "") {
				$('#passwordSpan').html('Please, enter password..!');
				document.loginform.password.focus();
				return false;
			} else if (document.loginform.password.value.match(/^[\s]+$/)) {
				$('#passwordSpan').html('Please, enter password..!');
				document.loginform.password.value = "";
				document.loginform.password.focus();
				return false;
			}
			
			
		}
		</script>
</body>
</html>
