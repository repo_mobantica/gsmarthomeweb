
                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li>
                                <a href="Dashboard"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                            </li>
                            
                            <li>
                                <a href="UserMaster"><i class="fa fa-user fa-fw"></i> Customer Master</a>
                            </li>
                            
                            <li>
                                <a href="HomeMaster"><i class="fa fa-home fa-fw"></i> Home Master</a>
                            </li>
                            
                            <li>
                                <a href="RoomMaster"><i class="fa fa-star fa-fw"></i> Room Master</a>
                            </li>
                            
                            <li>
                                <a href="PanelMaster"><i class="fa fa-cube fa-fw"></i> Panel Master</a>
                            </li>
                            
                            <li>
                                <a href="SwitchMaster"><i class="fa fa-diamond fa-fw"></i> Switch Master</a>
                            </li>
                              
                            <li>
                                <a href="DeviceMaster"><i class="fa fa-codepen fa-fw"></i> Device Master</a>
                            </li>
                            
                            <li>
                                <a href="IOTProductMaster"><i class="fa fa-table fa-fw"></i> IOTProduct Master</a>
                            </li>
                            
                            <li>
                                <a href="AdminMaster"><i class="fa fa-table fa-fw"></i> Admin Master</a>
                            </li>
                            
                            <li>
                                <a href="#"><i class="fa fa-comment fa-fw"></i> Complaint Master<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                      <a href="ComplaintMaster"><i class="fa fa-hourglass-o fa-fw"></i>New Complaint Master</a>
                                    </li>
                                    <li>
                                      <a href="InprogressComplaintMaster"><i class="fa fa-hourglass-half fa-fw"></i>Inprogress Complaint</a>
                                    </li>
                                    <li>
                                      <a href="CompletedComplaintMaster"><i class="fa fa-hourglass fa-fw"></i>Completed Complaint</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                            
                            <li>
                                <a href="EnquiryMaster"><i class="fa fa-hand-paper-o fa-fw"></i> Enquiry Master</a>
                            </li>
                            
                            <li>
                                <a href="FeedbackMaster"><i class="fa fa-comments fa-fw"></i> Feedback Master</a>
                            </li>
                            
                            
                            <li>
                                <a href="VersionMaster"><i class="fa fa-star fa-fw"></i> Version Master</a>
                            </li>
                            
                            
                            
                        </ul>
                    </div>
                </div>
