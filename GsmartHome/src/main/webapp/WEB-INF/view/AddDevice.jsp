<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Add Device</title>

<!-- Bootstrap Core CSS -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="resources/css/metisMenu.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="resources/css/startmin.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="resources/css/font-awesome.min.css" rel="stylesheet"
	type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>
<body onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("userName") == null || session.getAttribute("password") == null ) 
    			{
    				response.sendRedirect("");
    			} 
    		}
	%>
	<div id="wrapper">

		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

			<%@ include file="header.jsp"%>

			<%@ include file="menu.jsp"%>

		</nav>

		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<h3 class="page-header">Add Device Id</h3>
					</div>
				</div>
				<!-- /.row -->

				<form name="deviceform"
					action="${pageContext.request.contextPath}/AddDevice"
					onSubmit="return validate()" method="post">

					<div class="panel panel-default">
						<div class="row">
							<div class="col-lg-12">

                            <div class="panel panel-green"></div>

								<div class="panel-body">
									<div class="row">
										<div class="col-lg-6">


											<div class="panel-body">
												<div class="row">

													<div class="col-lg-3"></div>
													<div class="col-lg-8">
														<div class="form-group">
															<label>Device Id</label> <input class="form-control"
																id="deviceId" name="deviceId" placeholder="Device Id">
															<span id="deviceIdSpan" style="color: #FF0000"></span>
														</div>
													</div>
												</div>
											</div>


											<div class="panel-body">
												<div class="row">

													<div class="col-lg-3"></div>

													<div class="col-lg-3">
														<div class="col-lg-3">
															<a href="DeviceMaster"><button type="button"
																	class="btn btn-block btn-primary" value="Back"
																	style="width: 80px">Back</button></a>
														</div>
													</div>

													<div class="col-lg-3">


														<button type="reset" class="btn btn-default" value="reset"
															style="width: 80px">Reset</button>
													</div>

													<div class="col-lg-3">
														<button type="submit" class="btn btn-success"
															style="width: 80px">Save</button>
													</div>

												</div>
											</div>

										</div>
									</div>
								</div>


							</div>
						</div>
					</div>


				</form>



				<!-- /.row -->
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script src="resources/js/jquery.min.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="resources/js/bootstrap.min.js"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script src="resources/js/metisMenu.min.js"></script>

	<!-- Custom Theme JavaScript -->
	<script src="resources/js/startmin.js"></script>

	<script>

function init()
{

	document.deviceform.deviceId.value="";
	document.deviceform.deviceId.focus();
	
}
function validate()
{ 

	$('#deviceIdSpan').html('');
	if(document.deviceform.deviceId.value=="")
	{
		 $('#deviceIdSpan').html('Please, enter  Device Id..!');
		document.deviceform.deviceId.focus();
		return false;
	}
	else if(document.deviceform.deviceId.value.match(/^[\s]+$/))
	{
		 $('#deviceIdSpan').html('Please, enter Device Id..!');
		document.deviceform.deviceId.value="";
		document.deviceform.deviceId.focus();
		return false;	
	}
	
}
</script>
</body>
</html>
